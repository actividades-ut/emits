export interface IuserInfo {
  id: number,
  name: string,
  lastName: string,
  email: string,
  age: number,
}