import type { IuserInfo } from "@/interfaces/IUserInfo"

const Users: IuserInfo[] = [
  {
    id: 1,
    name: 'Ricardo',
    lastName: 'Cruz',
    email: 'elprofe@gmail.com',
    age: 24
  },
  {
    id: 2,
    name: 'Ana',
    lastName: 'Teresa',
    email: 'banana-atrot@gmail.com',
    age: 28
  },
  {
    id: 3,
    name: 'María',
    lastName: 'De las nieves',
    email: 'antonieta@gmail.com',
    age: 28
  },
  {
    id: 4,
    name: 'Peter',
    lastName: 'Parker',
    email: 'spider-man@gmail.com',
    age: 28
  },
  {
    id: 5,
    name: 'Luffy',
    lastName: 'D',
    email: 'estatuadebronce@gmail.com',
    age: 28
  },
]

export default Users